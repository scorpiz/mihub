<?php

class classe {
    use Genos;
    public $id;
    public $nom;

    public function __construct() {
        $this->id = 0;
        $this->nom = "";
    }

    public static function getClasses() {
        $classe = new classe;
        $req = "SELECT  c.id               as id,
                        c.nom              as nom,
                        COUNT(u.id_classe) as nb_etudiants
                FROM classe c, user u
                WHERE u.id_classe = c.id
                ";

        $fields = array("id", "nom", "nb_etudiants");
        $res = $classe->StructList($req, $fields, '', 'json');
        return $res;
    }

}