<?php 

class user {
	use Genos;

	public $id;
	public $nom;
	public $prenom;
	public $telephone;
	public $email;
	public $adresse;
	public $ville;
	public $login;
	public $password;
	public $role;
	public $id_classe;

	public function __construct(){
		$this->id = 0;
		$this->nom = "";
		$this->prenom = "";
		$this->telephone = "";
		$this->email = "";
		$this->adresse = "";
		$this->ville = "";
		$this->login = "";
		$this->password = "";
		$this->role = 0;
		$this->id_classe = 0;
	}

	public static function GetUsers($username, $password) {
		$user = new user;
		$req = 	"SELECT *
				 FROM user 
				 WHERE username = :username AND password = :password
				";
		$fields = array("id", "username", "password", "role");

		$bind = array("username" => $username, "password" => $password);
		$res = $user->StructList($req, $fields, $bind);
		return $res;
	}

	public static function getCurrentUserInfos($id) {
		$user = new user;
		$req = 	"SELECT u.id 		as id,
						u.nom 		as nom,
						u.prenom 	as prenom,
						u.telephone as telephone,
						u.email 	as email,
						u.adresse   as adresse,
						u.ville   	as ville,
						u.id_classe as id_classe,
						c.nom 		as classe
				FROM user u
				LEFT JOIN classe c ON c.id = u.id
				WHERE u.id = :id
				";
		$fields = array("id", "nom", "prenom", "telephone", "email", "adresse", "ville", "id_classe", "classe");
		$bind = array("id" => $id);
		$res = $user->StructList($req, $fields, $bind);
		return $res;
	}

	public static function getUserInfosByRole($role) {
		$user = new user;
		$req = 	"SELECT u.id 		as id,
						u.nom 		as nom,
						u.prenom 	as prenom,
						u.telephone as telephone,
						u.email 	as email,
						u.adresse   as adresse,
						u.ville   	as ville,
						u.username  as username,
						u.password  as password,
						u.role   	as role,
						u.id_classe as id_classe,
						c.nom 		as classe
				FROM user u
				LEFT JOIN classe c ON c.id = u.id
				WHERE u.role = :role
				";
		$fields = array("id", "nom", "prenom", "telephone", "email", "adresse", "ville", "username", "password", "role", "id_classe", "classe");
		$bind = array("role" => $role);
		$res = $user->StructList($req, $fields, $bind, "json");
		return $res;
	}

}