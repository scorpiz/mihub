<?php

class event {
    use Genos;

    public $id;
    public $titre;
    public $date_event;

    public function __construct() {
        $this->id = 0;
        $this->titre = "";
        $this->date_event = date("");
    }

    public static function getEvents() {
        $event = new event;

        $req = "SELECT * FROM event";
        $fields = $event->FieldList();
        $res = $event->StructList($req, $fields, '', 'json');
        return $res;
    }
}