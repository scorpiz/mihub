<?php
include('0-config/config-genos.php');
session_start();

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$_POST = json_decode(file_get_contents('php://input'), true);
if(isset($_POST) && !empty($_POST)) {

  $username = $_POST['username'];
  $password = $_POST['password'];
  $response = user::getUsers($username, $password);

  if($response && $response[0]) {
    $user = $response[0];
    switch($user['role']){ 
      case 1:
        $_SESSION['id'] = $user['id'];
        $_SESSION['role'] = $user['role'];
        ?>
        {
            "success": true,
            "role" : "utilisateur" 
        }
        <?php
        
        break;
      case 2:
        $_SESSION['id'] = $user['id'];
        $_SESSION['role'] = $user['role'];
        ?>
        {
            "success": true,
            "role" : "entreprise" 
        }
        <?php
        break;
      case 3:
        $_SESSION['id'] = $user['id'];
        $_SESSION['role'] = $user['role'];
        ?>
        {
            "success": true,
            "role" : "admin" 
        }
        <?php
        break;
      }
            
  } else {
    ?>
    {
        "success": false,
        "message" : "mauvais identifiants" 
    }
    <?php
  }
} else {
    ?>
    {
        "success": false,
        "message" : "POST only" 
    }
    <?php
}
?>