<?php 
include('0-config/config-genos.php');

session_start();

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


if(isset($_SESSION['id'])){
    $currentUser = user::getCurrentUserInfos($_SESSION['id']);
    
    switch($_SESSION['role']) {
        case 1:
            $_SESSION['role'] = "utilisateur";
        break;
        case 2:
            $_SESSION['role'] = "entreprise";
        break;
        case 3:
            $_SESSION['role'] = "admin";
        break;
    }

    $arr = array("isLoggedIn" => true, "user"=> $currentUser[0], "role" => $_SESSION['role']);
    $json = json_encode($arr);
    echo $json;
} else {
    $arr = array("isLoggedIn" => false);
    $json = json_encode($arr);
    echo $json;
}